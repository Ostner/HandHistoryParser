//
//  Extensions.swift
//  HandHistoryParser
//
//  Created by Tobias Ostner on 7/28/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import Foundation

extension Scanner {
    func scanUpTo(string: String) -> String? {
        var result: NSString?
        return scanUpTo(string, into: &result) ? result! as String : nil
    }

    func scanUpTo(characters: CharacterSet) -> String? {
        var result: NSString?
        return scanUpToCharacters(from: characters, into: &result) ? result! as String : nil
    }

    func scanInt() -> Int? {
        let result: UnsafeMutablePointer<Int>? = UnsafeMutablePointer<Int>.allocate(capacity: 1)
        defer { result?.deallocate(capacity: 1) }
        return scanInt(result) ? result?.pointee : nil
    }

    func scanDouble() -> Double? {
        let result: UnsafeMutablePointer<Double>? = UnsafeMutablePointer<Double>.allocate(capacity: 1)
        defer { result?.deallocate(capacity: 1) }
        return scanDouble(result) ? result?.pointee : nil
    }

    func scanCharacters(_ characters: CharacterSet) -> String? {
        var result: NSString?
        return scanCharacters(from: characters, into: &result) ? result! as String : nil
    }

    func scanWord() -> String? {
        return scanUpTo(characters: .whitespacesAndNewlines)
    }

    func scanToEndOfLine() -> String? {
        return scanUpTo(characters: .newlines)
    }
}

extension String {
    var trimmed: String {
        return trimmingCharacters(in: .whitespaces)
    }
}
