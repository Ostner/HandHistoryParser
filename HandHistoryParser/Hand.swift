//
//  Hand.swift
//  HandHistoryParser
//
//  Created by Tobias Ostner on 7/28/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import Foundation

enum Host: String {
    case PokerStars
    case Unknown
}

enum HandType: String {
    case normal
    case zoom
}

enum Variant {
    case holdem
    case omaha
}

extension Variant {
    init?(string: String) {
        let str = string.lowercased()
        switch str {
        case "hold'em": self = .holdem
        case "omaha": self = .omaha
        default: return nil
        }
    }
}

enum Betting {
    case limit
    case noLimit
    case potLimit
}

extension Betting {
    init?(string: String) {
        let str = string.lowercased()
        switch str {
        case "limit": self = .limit
        case "no limit": self = .noLimit
        case "pot limit": self = .potLimit
        default: return nil
        }
    }
}

struct Blinds {
    let bb: Double
    let sb: Double
}

extension Blinds: Equatable {
    static func ==(lhs: Blinds, rhs: Blinds) -> Bool {
        return lhs.bb == rhs.bb && lhs.sb == rhs.sb
    }
}

struct Header {
    let host: Host
    let type: HandType
    let ident: Int
    let variant: Variant
    let betting: Betting
    let blinds: Blinds
    let date: Date
}

struct Hand {
    let host: Host
    let type: HandType
    let ident: Int
    let variant: Variant
    let betting: Betting
    let blinds: Blinds
    let date: Date
}

extension Hand {
    init(with header: Header) {
        host = header.host
        type = header.type
        ident = header.ident
        variant = header.variant
        betting = header.betting
        blinds = header.blinds
        date = header.date
    }
}

extension Hand: Equatable {
    static func ==(lhs: Hand, rhs: Hand) -> Bool {
        return lhs.host == rhs.host
    }
}
