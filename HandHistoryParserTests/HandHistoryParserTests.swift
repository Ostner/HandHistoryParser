//
//  HandHistoryParserTests.swift
//  HandHistoryParserTests
//
//  Created by Tobias Ostner on 7/28/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import XCTest
@testable import HandHistoryParser

class HandHistoryParserTests: XCTestCase {

    func testParseHost_withoutLeadingWhitespaces() {
        let hh = "PokerStars Hand"
        let parser = HandHistoryParser(string: hh)
        XCTAssertEqual(try! parser.parseHost(), .PokerStars)
    }

    func testParseHost_withLeadingWhitespaces() {
        let hh = "    PokerStars Hand"
        let parser = HandHistoryParser(string: hh)
        XCTAssertEqual(try! parser.parseHost(), .PokerStars)
    }

    func testParseHost_malformed() {
        let parser = HandHistoryParser(string: "")
        XCTAssertThrowsError(try parser.parseHost())
    }

    func testParseHandType_zoom() {
        let hh = "Zoom Hand #111111111"
        let parser = HandHistoryParser(string: hh)
        XCTAssertEqual(try! parser.parseHandType(), .zoom)
    }

    func testParseHandType_zoomWithLeadingWhitespaces() {
        let hh = "    Zoom Hand #111111111"
        let parser = HandHistoryParser(string: hh)
        XCTAssertEqual(try! parser.parseHandType(), .zoom)
    }

    func testParseHandType_normal() {
        let hh = "Hand #1111111111"
        let parser = HandHistoryParser(string: hh)
        XCTAssertEqual(try! parser.parseHandType(), .normal)
    }

    func testParseHandType_malformed() {
        let parser = HandHistoryParser(string: "")
        XCTAssertThrowsError(try parser.parseHandType())
    }

    func testParseId_noLeadingWhitespaces() {
        let hh = "#1111111111111:"
        let parser = HandHistoryParser(string: hh)
        XCTAssertEqual(try! parser.parseId(), 1111111111111)
    }

    func testParseId_withLeadingWhitespaces() {
        let hh = "     #1111111111112:"
        let parser = HandHistoryParser(string: hh)
        XCTAssertEqual(try! parser.parseId(), 1111111111112)
    }

    func testParseId_malformed() {
        let parser = HandHistoryParser(string: "")
        XCTAssertThrowsError(try parser.parseId())
    }

    func testParseVariant_omaha() {
        let hh = "Omaha No Limit"
        let parser = HandHistoryParser(string: hh)
        XCTAssertEqual(try! parser.parseVariant(), .omaha)
    }

    func testParseVariant_omahaWithLeadingVestiges() {
        let hh = ":    Omaha Pot Limit"
        let parser = HandHistoryParser(string: hh)
        XCTAssertEqual(try! parser.parseVariant(), .omaha)
    }

    func testParseVariant_holdem() {
        let hh = "Hold'em No Limit"
        let parser = HandHistoryParser(string: hh)
        XCTAssertEqual(try! parser.parseVariant(), .holdem)
    }

    func testParseVariant_malformed() {
        let parser = HandHistoryParser(string: "")
        XCTAssertThrowsError(try parser.parseVariant())
    }

    func testParseBetting_potLimit() {
        let hh = "Pot Limit ($1.00/$2.00)"
        let parser = HandHistoryParser(string: hh)
        XCTAssertEqual(try! parser.parseBetting(), .potLimit)
    }

    func testParseBetting_noLimit() {
        let hh = "No Limit ($1.00/$2.00)"
        let parser = HandHistoryParser(string: hh)
        XCTAssertEqual(try! parser.parseBetting(), .noLimit)
    }

    func testParseBetting_limit() {
        let hh = "Limit ($1.00/$2.00)"
        let parser = HandHistoryParser(string: hh)
        XCTAssertEqual(try! parser.parseBetting(), .limit)
    }

    func testParseBetting_malformed() {
        let parser = HandHistoryParser(string: "")
        XCTAssertThrowsError(try parser.parseBetting())
    }

    func testParseBlinds() {
        let hh = "($1.00/$2.00)"
        let parser = HandHistoryParser(string: hh)
        let blinds = Blinds(bb: 2, sb: 1)
        XCTAssertEqual(try! parser.parseBlinds(), blinds)
    }

    func testParseBlinds_malformed() {
        let parser = HandHistoryParser(string: "")
        XCTAssertThrowsError(try parser.parseBlinds())
    }

    func testParseDate() {
        let hh = "2017/07/23 12:52:56 ET"
        let parser = HandHistoryParser(string: hh)
        let calendar = Calendar.current
        var components = DateComponents()
        components.calendar = calendar
        components.year = 2017
        components.month = 7
        components.day = 23
        components.hour = 12
        components.minute = 52
        components.second = 56
        components.timeZone = TimeZone(abbreviation: "EST")
        let date = calendar.date(from: components)
        XCTAssertEqual(try! parser.parseDate(), date)
    }

    func testParseDate_malformed() {
        let parser = HandHistoryParser(string: "")
        XCTAssertThrowsError(try parser.parseDate())
    }

    func testParseHeader() {
        let hh = "PokerStars Zoom Hand #173408239039:  Hold'em No Limit ($1.00/$2.00) - 2017/07/23 12:52:56 ET"
        let parser = HandHistoryParser(string: hh)
        XCTAssertNoThrow(try parser.parseHeader())
    }

}
