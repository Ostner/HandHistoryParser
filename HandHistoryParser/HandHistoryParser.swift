//
//  Parser.swift
//  HandHistoryParser
//
//  Created by Tobias Ostner on 7/28/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import Foundation

enum ParsingError: Error {
    case malformedHost
    case malformedHandType
    case malformedId
    case malformedVariant
    case malformedBetting
    case malformedBlinds
    case malformedDate
}

class HandHistoryParser {

    let scanner: Scanner

    let specialCharacters = CharacterSet(charactersIn: "#:()$/-")

    let dateFormatter: DateFormatter = {
        let df = DateFormatter()
        df.timeZone = TimeZone(abbreviation: "EST")
        df.dateFormat = "yyyy/MM/dd HH:mm:ss"
        return df
    }()

    init(string: String) {
        scanner = Scanner(string: string)
        scanner.charactersToBeSkipped = (scanner.charactersToBeSkipped ?? CharacterSet()).union(specialCharacters)
    }

    func parseHeader() throws -> Header {
        let host = try parseHost()
        let handType = try parseHandType()
        let id = try parseId()
        let variant = try parseVariant()
        let betting = try parseBetting()
        let blinds = try parseBlinds()
        let date = try parseDate()
        return Header(
            host: host,
            type: handType,
            ident: id,
            variant: variant,
            betting: betting,
            blinds: blinds,
            date: date)
    }

    func parseHost() throws -> Host {
        guard let hostString = scanner.scanWord(),
              let host = Host(rawValue: hostString)
        else { throw(ParsingError.malformedHost) }
        return host
    }

    func parseHandType() throws -> HandType {
        guard let type = scanner.scanWord() else { throw(ParsingError.malformedHandType) }
        if type == "Hand" {
            return .normal
        }
        guard let _ = scanner.scanWord(),
              let handType = HandType(rawValue: type.lowercased())
        else { throw(ParsingError.malformedHandType) }
        return handType
    }

    func parseId() throws -> Int {
        guard let id = scanner.scanInt() else { throw(ParsingError.malformedId)}
        return id
    }

    func parseVariant() throws -> Variant {
        guard let name = scanner.scanWord(),
              let variant = Variant(string: name)
        else { throw(ParsingError.malformedVariant) }
        return variant
    }

    func parseBetting() throws -> Betting {
        guard let word1 = scanner.scanWord()?.lowercased() else { throw(ParsingError.malformedBetting) }
        if word1 == "limit" {
            return .limit
        }
        guard let word2 = scanner.scanWord()?.lowercased(),
              let betting = Betting(string: "\(word1) \(word2)")
        else { throw(ParsingError.malformedBetting) }
        return betting
    }

    func parseBlinds() throws -> Blinds {
        guard let sb = scanner.scanDouble(),
              let bb = scanner.scanDouble()
        else { throw(ParsingError.malformedBlinds) }
        return Blinds(bb: bb, sb: sb)
    }

    func parseDate() throws -> Date {
        guard let day = scanner.scanWord(),
              let time = scanner.scanWord(),
              let _ = scanner.scanWord(), // timezone abbreviation
              let date = dateFormatter.date(from: "\(day) \(time)")
        else { throw(ParsingError.malformedDate) }
        return date
    }

}
