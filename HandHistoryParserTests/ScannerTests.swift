//
//  ScannerTests.swift
//  HandHistoryParserTests
//
//  Created by Tobias Ostner on 7/30/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import XCTest
@testable import HandHistoryParser

class ScannerTests: XCTestCase {
    
    func testScanWord_noLeadingWhitespace() {
        let scanner = Scanner(string: "word")
        XCTAssertEqual(scanner.scanWord()!, "word")
        print(scanner.scanLocation)
    }

    func testScanWord_withLeadingWhitespaces() {
        let scanner = Scanner(string: "  word  ")
        XCTAssertEqual(scanner.scanWord(), "word")
    }

    func testScanWord_withSpecialCharacters() {
        let scanner = Scanner(string: "123:")
        XCTAssertEqual(scanner.scanWord(), "123:")
    }

    func testScanInteger() {
        let scanner = Scanner(string: "42")
        XCTAssertEqual(scanner.scanInt(), 42)
    }

    func testScanDouble() {
        let scanner = Scanner(string: "0.05")
        XCTAssertEqual(scanner.scanDouble(), 0.05)
    }

}
